import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (anio,
                                     apellidoM,
                                     apellidoP,
                                     correo,
                                     cp,
                                     fechaNacimiento,
                                     grupoCallback,
                                     idCampana,
                                     idPagina,
                                     marca,
                                     nombre,
                                     sexo,
                                     submarca,
                                     modelo,
                                     detalle,
                                     telefono,
                                     telefonoAS,
                                     from,
                                     precio) {

  return axios({
    method: "post",
    url: configDB.baseUrl+  '/nuevaSolicitudAutoCRM',
    data: {
      anio,
      apellidoM,
      apellidoP,
      correo,
      cp,
      fechaNacimiento,
      grupoCallback,
      idCampana,
      idPagina,
      marca,
      nombre,
      sexo,
      submarca,
      modelo,
      detalle,
      telefono,
      telefonoAS,
      from,
      precio
    }
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default cotizacionService


