
const dbService = {
  //baseUrl: 'http://localhost:8080/ws-rest/servicios',
  baseUrl: 'https://mejorsegurodeauto.mx/ws-rest/servicios',
  baseAutos: 'https://mejorsegurodeauto.mx/ws-autos/servicios'
}

export default dbService
