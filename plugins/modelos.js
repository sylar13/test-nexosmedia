import autosService from './ws-autos'

const modelosService ={}

modelosService.search=function (aseguradora, marca) {
  return autosService.get('/modelos',{
    params:{aseguradora,marca}
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default modelosService
