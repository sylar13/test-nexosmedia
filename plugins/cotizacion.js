import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (aseguradora,
                                     clave,
                                     cp,
                                     descripcion,
                                     descuento,
                                     edad,
                                     fechaNacimiento,
                                     genero,
                                     marca,
                                     modelo,
                                     movimiento,
                                     paquete,
                                     servicio) {

  return axios({
    method: "post",
    url: configDB.baseAutos + '/cotizar',
    data: {
      aseguradora,
      clave,
      cp,
      descripcion,
      descuento,
      edad,
      fechaNacimiento,
      genero,
      marca,
      modelo,
      movimiento,
      paquete,
      servicio
    }
  })
    .then(res => res.data)
    .catch(err => console.error("Problema al cotizar con "+ aseguradora+" "+err));
}
export default cotizacionService


