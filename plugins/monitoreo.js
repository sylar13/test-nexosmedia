import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (anio,
                                     aseguradora,
                                     cotizo,
                                     descripcion,
                                     detalle,
                                     idCampania,
                                     idEjecutivo,
                                     idPagina,
                                     idSolicitud,
                                     marca,
                                     operacion,
                                     subdescripcion
                                     ) {

  return axios({
    method: "post",
    url: configDB.baseUrl + '/saveMonitoreoLanding',
    data: {
      anio,
      aseguradora,
      cotizo,
      descripcion,
      detalle,
      idCampania,
      idEjecutivo,
      idPagina,
      idSolicitud,
      marca,
      operacion,
      subdescripcion
    }
  })
    .then(res => res.data)
    .catch(err => console.error("Error al guardar el monitoreo "+err));
}
export default cotizacionService


