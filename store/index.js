import Vuex from 'vuex'
import database from '~/plugins/saveData'
import monitoreo from '~/plugins/monitoreo'
import cotizacion from '~/plugins/cotizacion'
import saveProspectoCRMService from '~/plugins/CRM'
import wsSinCotizacion from '~/plugins/wsSinCotizacion'
const createStore = () => {
    const conexionCRMService =  new saveProspectoCRMService();
    return new Vuex.Store({
        state: {
            ambientePruebas: false,
            cargandocotizacion: false,
            msj: false,
            cotizacionesAmplia: [],
            cotizacionesLimitada: [],
            config: {
                tipoPaquete: 0,
                aseguradora: '',
                cotizacion: true,
                emision: false,
                descuento: 0,
                telefonoAS: '',
                grupoCallback: '',
                from: '',
                idPagina: 0,
                idAseguradora: 0
            },
            ejecutivo: {
                nombre: '',
                correo: '',
                id: 0
            },
            formData: {
                aseguradora: '',
                marca: '',
                modelo: '',
                descripcion: '',
                subDescripcion: '',
                detalle: '',
                detalleid: '',
                codigoPostal: '',
                nombre: '',
                apellidoPaterno: '',
                apellidoMaterno: '',
                telefono: '',
                correo: '',
                edad: '',
                fechaNacimiento: '',
                genero: '',
                precio: ''
            },
            solicitud: {},
            cotizacion: {
                "Cliente": {
                    "FechaNacimiento": null,
                    "Genero": null,
                    "Direccion": {
                        "CodPostal": null,
                        "Calle": null,
                        "Colonia": null,
                        "Poblacion": null,
                        "Ciudad": null,
                        "Pais": null
                    },
                    "Edad": null,
                    "TipoPersona": null,
                    "Nombre": null,
                    "ApellidoPat": null,
                    "ApellidoMat": null,
                    "RFC": null,
                    "CURP": null,
                    "Telefono": null,
                    "Email": null
                },
                "Cotizacion": {
                    "PrimaTotal": 0
                },
                "Emision": {
                    "PrimaTotal": null,
                },
                "Vehiculo": {
                    "Descripcion": null,
                    "Marca": null,
                    "Modelo": null,
                },
                "Aseguradora": "-",
                "Coberturas": [
                    {
                        "DanosMateriales": "-",
                        "DanosMaterialesPP": "-",
                        "RoboTotal": "-",
                        "RCBienes": "-",
                        "RCPersonas": "-",
                        "RC": "-",
                        "RCFamiliar": "-",
                        "RCExtension": "-",
                        "RCExtranjero": "-",
                        "RCPExtra": "-",
                        "AsitenciaCompleta": "-",
                        "DefensaJuridica": "-",
                        "GastosMedicosOcupantes": "-",
                        "MuerteAccidental": "-",
                        "GastosMedicosEvento": "-",
                        "Cristales": "-",
                    }
                ],
            },
            servicios: {
                servicioDB: 'http://138.197.128.236:8081/ws-autos/servicios'
            },
            prospecto : {
                numero: '',
                correo: '',
                nombre: '',
                sexo: '',
                edad: ''
            },
            productoSolicitud: {
                idProspecto: '',
                idTipoSubRamo: 1,
                datos: ''
            },
            cotizaciones: {
                idProducto: '',
                idPagina: '',
                idMedioDifusion: '',
                idEstadoCotizacion: 1,
                idTipoContacto: 1
            },
            cotizacionAli: {
                idCotizacion: '',
                idSubRamo: '',
                peticion: '',
                respuesta: '',
            },
            solicitudes: {
                idCotizacionAli: '',
                idEmpleado: '',
                idEstadoSolicitud: 1,
                idEtiquetaSolicitud: 1,
                idFlujoSolicitud: 1 ,
                comentarios: ''
            }
            //cotizaciones: []
        },
        mutations: {
            saveData: function (state) {
                console.log('Guardando Datos');
                database.search(
                    state.formData.modelo,
                    state.formData.apellidoMaterno,
                    state.formData.apellidoPaterno,
                    state.formData.correo,
                    state.formData.codigoPostal,
                    state.formData.fechaNacimiento,
                    state.config.grupoCallback,
                    state.config.idCampana,
                    state.config.idPagina,
                    state.formData.marca,
                    state.formData.nombre,
                    state.formData.genero,
                    state.formData.descripcion,
                    state.formData.subDescripcion,
                    state.formData.detalle,
                    state.formData.telefono,
                    state.config.telefonoAS,
                    state.config.from,
                    state.formData.precio)
                    .then(resp => {
                        state.solicitud = resp;
                        state.ejecutivo.nombre = resp.nombreEjecutivo;
                        state.ejecutivo.correo = resp.mailEjecutivo;
                        state.ejecutivo.id = resp.idEjecutivo;
                        this.commit('saveProspectoCRM');
                        this.commit('saveMonitoreo');
                    });      
            },
            saveMonitoreo: function (state) {
                console.log('save Monitoreo');
                console.log('Cotizacion Completa');
                state.cargandocotizacion = true;
                monitoreo.search(
                    state.formData.modelo,
                    state.config.aseguradora,
                    (state.cotizacion.length > 0 ? 'SI' : 'NO'),
                    state.formData.descripcion,
                    state.formData.detalle,
                    state.config.idCampana,
                    state.ejecutivo.id,
                    state.config.idPagina,
                    state.solicitud.idSolicitud,
                    state.formData.marca,
                    "COTIZACION",
                    state.formData.subDescripcion,
                    state.formData.precio)
                    .then(resp => {
                    });
            },
            cotizar: function (state) {
                cotizacion.search(
                    state.config.aseguradora,
                    state.formData.detalleid,
                    state.formData.codigoPostal,
                    state.formData.descripcion,
                    state.config.descuento,
                    state.formData.edad,
                    ("01/01/" + (new Date().getFullYear() - state.formData.edad).toString()).toString("yyyy/MM/dd"),
                    (state.formData.genero === 'M' ? 'MASCULINO' : 'FEMENINO'),
                    state.formData.marca,
                    state.formData.modelo,
                    'cotizacion',
                    'AMPLIA',
                    'PARTICULAR'
                ).then(resp => {
                    console.log("Se esta Cotizando va bien");
                    state.cotizacion = resp;
                    if (typeof resp != "string") {
                        if (Object.keys(resp.Cotizacion).includes('PrimaTotal') && resp.Cotizacion.PrimaTotal != null && resp.Cotizacion.PrimaTotal != '') {
                            state.formData.precio = state.cotizacion.Cotizacion.PrimaTotal;
                            this.commit('saveData');
                            console.log('Se mando Correo');
                            this.commit('limpiarCoberturas');
                        }
                        else {
                            this.commit('saveData');
                            state.msj = true
                            console.log("Sin resultados");
                            console.log(state.config.aseguradora + " no trajo precios D:");
                            

                            var jsonWsPorAseguradora = {
                                aseguradora: state.config.aseguradora,
                                idDetalle: state.formData.detalleid,
                                cp: state.formData.codigoPostal,
                                detalle: state.formData.detalle,
                                descuento: state.config.descuento,
                                edad: state.formData.edad,
                                fechaNacimiento: state.formData.fechaNacimiento,
                                genero: state.formData.genero,
                                marca: state.formData.marca,
                                modelo: state.formData.modelo,
                                cotizacion: 'cotizacion',
                                cobertira: 'AMPLIA',
                                tipo_vehiculo: 'PARTICULAR'
                            }
                            var jsonPeticionWs = {
                                id_cotizacion: state.config.idCotizacion,
                                num_cotizacion: state.config.numCotizacionUsuario,
                                id_aseguradora: state.config.id_aseguradora,
                                peticion: JSON.stringify(jsonWsPorAseguradora),
                                respuesta: JSON.stringify(resp),
                                statusCotizacion: 2
                            };
                            wsSinCotizacion.sinCotizacion(
                                JSON.stringify(jsonPeticionWs)
                            ).then(respSinCotizacion => {
                            });
                        }
                    }
                    else {
                        this.commit('saveData');
                    }
                });
            },
            limpiarCoberturas(state) {
                if (state.cotizacion.Coberturas) {
                    for (var c in state.cotizacion.Coberturas[0]) {
                        if (state.cotizacion.Coberturas[0][c] === "-") {
                            delete state.cotizacion.Coberturas[0][c];
                        }
                    }
                }
            },
            saveProspectoCRM: function (state) {
                state.prospecto.numero = state.formData.telefono;
                state.prospecto.correo = state.formData.correo;
                state.prospecto.nombre = state.formData.nombre;
                state.prospecto.sexo = state.formData.genero;
                state.prospecto.edad = state.formData.edad;
                conexionCRMService.validarCorreo(state.prospecto.correo).then(
                    correoExist => {
                        if (typeof correoExist.data.correo === 'string') {
                            state.productoSolicitud.idProspecto = correoExist.data.id;
                            this.commit('saveProductoSolicitudCRM');
                        } else {
                            conexionCRMService.postProspecto(state.prospecto).then(resp => {
                                console.log(resp.data.id);
                                state.productoSolicitud.idProspecto = resp.data.id;
                                this.commit('saveProductoSolicitudCRM');
                            })
                        }
                    }
                )
            },
            saveProductoSolicitudCRM: function (state) {
                state.productoSolicitud.datos = JSON.stringify(state.formData);
                conexionCRMService.postProductoSolicitud(state.productoSolicitud).then( resp => {
                state.cotizaciones.idProducto = resp.data.id;
                this.commit('saveCotizacionesCRM');
                })
            },
            saveCotizacionesCRM: function (state) {
                state.cotizaciones.idMedioDifusion = state.config.idPagina;
                conexionCRMService.postCotizacion(state.cotizaciones).then( resp => {
                state.cotizacionAli.idCotizacion = resp.data.id;
                this.commit('saveCotizacionesAli');
                })
            },
            saveCotizacionesAli: function (state) {
                state.cotizacionAli.idSubRamo = state.config.idAseguradoraCRM;
                state.cotizacionAli.peticion = JSON.stringify(state.formData);
                console.log(state.cotizacionAli);
                state.cotizacionAli.respuesta = '{"propiedad": "vacia"}';
                conexionCRMService.postCotizacionAli(state.cotizacionAli).then( resp => {
                    state.solicitudes.idCotizacionAli = resp.data.id;
                    this.commit('saveSolicitudes');
                })
            },
            saveSolicitudes: function (state) {
                state.solicitudes.idEmpleado = state.ejecutivo.id;
                console.log(state.solicitudes);
                conexionCRMService.postSolicitud(state.solicitudes).then( resp => {
                    console.log('CONEXION A TRIGARANTE2020 TERMINADA')
                })
            }
        }
    })
}
export default createStore
