module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'ahorraseguros',
    htmlAttrs: { lang: 'es-MX' },
    meta: [
      { charset: 'utf-8' },
      { hid: 'robots', name: 'robots', content: 'index, follow' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      { hid: 'description', name: 'description', content: 'ahorraseguros' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon.png' }
    ],
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3bd600' },
  /*npm
  ** Build configuration
  */
  build: {

    vendor: ['jquery', 'bootstrap'],
    /*
   ** Run ESLint on save
   */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  // include bootstrap css
  css: ['bootstrap/dist/css/bootstrap.css','static/css/styles.css'],
  plugins: ['~plugins/bootstrap.js',{ src: '~/plugins/filters.js', ssr: false }],
  modules: [
      ['@nuxtjs/google-tag-manager', { id: 'GTM-KRCFZQ' }],
      ['nuxt-validate', {lang: 'es'}]
  ],
  env: {
    baseUrl: 'https://mejorsegurodeauto.mx/ws-autos/servicios'
  },
  render: {
    http2:{ push: true },
    resourceHints:false,
    gzip:{ threshold: 9 }
  }
}